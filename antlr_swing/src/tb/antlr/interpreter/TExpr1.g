tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import java.util.HashMap;
import tb.antlr.symbolTable.LocalSymbols;
}

@members {
  HashMap<String, Double> variables = new HashMap<String, Double>();
  LocalSymbols localSymbols = new LocalSymbols();
}

prog    : 
        ( print
        | expr
        | LCURL {localSymbols.enterScope();}
        | RCURL {localSymbols.leaveScope();}
        )*
        ;

print   : PRINT_KEYWORD (e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
        ;
      
logicalExpression returns [Boolean out] 
        : ^(AND e1=logicalExpression e2=logicalExpression) {$out = $e1.out && $e2.out;}
        | ^(OR e1=logicalExpression e2=logicalExpression) {$out =$e1.out || $e2.out;}
        | INTEGER { $out = getBoolean($INTEGER.text);}
        ;

expr returns [Double out]
        
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = Math.pow($e1.out, $e2.out);}
        | ^(ASSIGN i1=IDENTIFIER   e2=expr) { variables.put($i1.text, $e2.out); $out = $e2.out; }
        | INTEGER                      {$out = getDouble($INTEGER.text);}
        ;

