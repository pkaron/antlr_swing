package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {

	protected LocalSymbols locals = new LocalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Double getDouble(String text) {
		return Double.parseDouble(text);
	}
	
	protected Boolean getBoolean(String text) {
		return Boolean.parseBoolean(text);
	}
	
	protected Boolean getBoolean(Double value) {
		if(value == 0.0) return false;
		else return true;
	}
}
