grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (statement | block )+ EOF!;


block
    : LCURL^ (statement | block)* RCURL!
    ;
    
statement
    : disjunction NL -> disjunction
    | print NL -> print
    | VAR IDENTIFIER NL -> ^(VAR IDENTIFIER)
    | if_stat NL -> if_stat
    | NL ->
    ;

if_stat
    : IF^ LP^ disjunction RP^ disjunction (ELSE^ disjunction)?
    ;

print
    : PRINT_KEYWORD disjunction
    ;

disjunction
    : conjunction (OR^ conjunction)*
    ;

conjunction
    : equality (AND^ equality)*
    ;

equality
    : comparison (equalityOperator^ comparison)*
    ;

equalityOperator
    : EQ
    | NEQ
    ;

comparison
    : additiveExpression (comparisonOperator^ additiveExpression)*
    ;

comparisonOperator
    : GTE 
    | LTE 
    | GT
    | LT
    ;
    
additiveExpression
    : multiplicativeExpression
      ( PLUS^ multiplicativeExpression
      | MINUS^ multiplicativeExpression
      )*
    ;

multiplicativeExpression
    : powerExpression
      ( MUL^ powerExpression
      | DIV^ powerExpression
      )*
    ;

powerExpression
    : atomicExpression (POW^ exponent)*
    ;
    
exponent
    : atomicExpression (POW^ exponent)*
    ;
    
atomicExpression
    : INTEGER
    | IDENTIFIER
    | LP! disjunction RP!
    | IDENTIFIER ASSIGN disjunction NL -> ^(ASSIGN IDENTIFIER disjunction)
    ;

VAR :'var';

PRINT_KEYWORD : 'print';
IF
  : 'if'
  ;
  
ELSE
  : 'else'
  ;
  
LCURL
  : '{'
  ;
  
RCURL
  : '}'
  ;
  
LP
	:	'('
	;

RP
	:	')'
	;

ASSIGN
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

POW: '**';

MUL
	:	'*'
	;

DIV
	:	'/'
	;

EQ: '==';
NEQ: '!=';
GTE : '>=';
LTE : '<=';
GT: '>';
LT: '<';

AND: '&&';
OR: '||';

LITERAL_FALSE: 'false';
LITERAL_TRUE: 'true';

IDENTIFIER : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INTEGER : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


