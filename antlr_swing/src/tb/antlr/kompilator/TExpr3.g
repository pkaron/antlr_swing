tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer counter = 0;
}
prog    : (e+=expression | d+=declaration)* -> program(name={$e},declarations={$d});

declaration  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> declareVariable(n={locals.getVariable($ID.text)})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expression
        : ^(PLUS  e1=expression e2=expression) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expression e2=expression) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expression e2=expression) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expression e2=expression) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expression) {locals.hasSymbol($ID.text);} -> assignVariable(p1={locals.getVariable($ID.text)},p2={$e2.st})
        | INT  {counter++;}                    -> int(i={$INT.text},j={counter.toString()})
        | ID {locals.getSymbol($ID.text);} -> id(i={locals.getVariable($ID.text)})
    ;
    